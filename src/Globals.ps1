﻿#--------------------------------------------------------------------------------------
#
# Déclaration des fonctions globales
#
#--------------------------------------------------------------------------------------
function Get-Office365OldCode
{
	$Maj = Get-Random -InputObject B, C, D, F, G, H, J, K, L, M, N, P, Q, R, S, T, V, W, X, Z
	$Min1 = Get-Random -InputObject a, e, i, o, u, y
	$Min2 = Get-Random -InputObject b, c, d, f, g, h, j, k, l, m, n, p, q, r, s, t, v, w, x, z
	$Min3 = Get-Random -InputObject a, e, i, o, u, y
	$Num1 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num2 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num3 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num4 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Pass = $Maj + $Min1 + $Min2 + $Min3 + $Num1 + $Num2 + $Num3 + $Num4
	
	return $Pass
}

function Get-Office365NewCode
{
	$Maj = Get-Random -InputObject B, C, D, F, G, H, J, K, L, M, N, P, Q, R, S, T, V, W, X, Y, Z
	$Min1 = Get-Random -InputObject a, e, i, o, u
	$Min2 = Get-Random -InputObject b, c, d, f, g, h, j, k, l, m, n, p, q, r, s, t, v, w, x, y, z
	$Num1 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num2 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num3 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num4 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num5 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Pass = $Maj + $Min1 + $Min2 + $Num1 + $Num2 + $Num3 + $Num4 + $Num5
	
	return $Pass
}

function Get-Office365EnhancedCode
{
	$Maj = Get-Random -InputObject B, C, D, F, G, H, J, K, L, M, N, P, Q, R, S, T, V, W, X, Z
	$Min1 = Get-Random -InputObject a, e, i, o, u, y
	$Min2 = Get-Random -InputObject b, c, d, f, g, h, j, k, l, m, n, p, q, r, s, t, v, w, x, z
	$Min3 = Get-Random -InputObject a, e, i, o, u, y
	$Spe = Get-Random -InputObject "!", "§", "$", "%", "&", "/", "(", ")", "=", "?", "}", "]", "[", "{", "@", "#", "*", "+", ",", ".", ";", "-", "_"
	$Num1 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num2 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num3 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Num4 = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	$Pass = $Maj + $Min1 + $Min2 + $Min3 + $Spe + $Num1 + $Num2 + $Num3 + $Num4
	
	return $Pass
}

function Get-HexaCode
{
	param
	(
		[int]$Length
	)
	$Pass = ""
	for ($h = 1; $h -le $Length; $h++)
	{
		$Char = Get-Random -InputObject 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f
		$Pass += $Char
	}
	
	return $Pass
}

function Get-AlphaNumSymCode
{
	param
	(
		[int]$Length
	)
	$Pass = ""
	$SpecialCharNumber = [System.Math]::Round($Length / 5)
	Add-Type -AssemblyName 'System.Web'
	$Pass = [System.Web.Security.Membership]::GeneratePassword($Length, $SpecialCharNumber)
	
	return $Pass
}

#--------------------------------------------------------------------------------------
#
# Déclaration des variables globales 
#
#--------------------------------------------------------------------------------------

# Mode de complexité par défaut
$global:InitialMode = "Office365 (amélioré)"