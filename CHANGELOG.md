# Serys - Suivi de version

## 4.1.0.0
- Ajout de la création de GUID

## 4.0.1.0
- Correction orthographique
- Génération aléatoire des exemples

## 4.0.0.0
- Refonte de l'interface principale
- Refonte de l'interface d'information
- Ajout des niveaux de complexité

## 3.0.1.0
- Correction d'un bug empêchant le démarrage de l'application lors de l'initialisation du module d'export TXT

## 3.0.0.0
- Réécriture complète du code
- Réécriture de l'interface
- Ajout de la fonction de génération multiple
- Ajout de la fonction export vers presse-papier
- Ajout de la fonction export vers TXT
- Ajout de la fonction export vers CSV

## 2.0.0.0
- Modification du code
- Ajout d'une interface

## 1.0.0.0
- Version originale