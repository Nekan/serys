<div align="center">![ShyrkaSystem](https://www.shyrkasystem.com/dl/logo-400.png)</div>

# Serys

![Status](https://img.shields.io/badge/Statut-Stable-brightgreen)  ![Version](https://img.shields.io/badge/Version-4.1.0.0-blue)  ![Environnement](https://img.shields.io/badge/Environnement-Windows-orange)  ![Architecture](https://img.shields.io/badge/Architecture-32/64%20Bits-blue)  ![Language](https://img.shields.io/badge/Language-Powershell-blueviolet)  ![Framework](https://img.shields.io/badge/Framework-.NET%202.0%20min-blueviolet)

<div align="center">![Serys](https://www.shyrkasystem.com/dl/serys/serys.png)</div>
Générateur de mots de passe aléatoires répondant à certaines normes de sécurité.

## Mode d'emploi

Le mode d'emploi est consultable à l'adresse : [ShyrkaSystem](https://www.shyrkasystem.com/doku.php?id=serys)

## Dossiers

###  bin

Contient l'application pour les systèmes Windows 32 ou 64 bits.

### images

Contient les images utilisées dans le programmes.

### export

Contient le script powershell non compilé. Utilisable directement depuis une console powershell.

### src

Contient les sources du programme, écrit avec SAPIEN PowerShell Studio.
