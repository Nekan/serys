# Serys - Suivi des contributions

## Images

- about.png par paomedia (Arnaud) - *https://github.com/paomedia* - Licence CC BY 4.0 *https://creativecommons.org/licenses/by/4.0/*
- clip.png par Trinh Ho - *https://dribbble.com/TrinhHo* - Licence Free for commercial use
- csv.png par VisualPharm - *http://www.visualpharm.com/* - Licence CC BY 4.0 *https://creativecommons.org/licenses/by/4.0/*
- run.png par DinosoftLabs - *https://www.iconfinder.com/dinosoftlabs* - Licence Free for commercial use
- serys.ico et serys.png par Hopstarter (Jojo Mendoza) - *http://hopstarter.deviantart.com/* - Licence CC BY-NC-ND 4.0 *https://creativecommons.org/licenses/by-nc-nd/4.0/*
- txt.png par VisualPharm - *http://www.visualpharm.com/* - Licence CC BY 4.0 *https://creativecommons.org/licenses/by/4.0/*